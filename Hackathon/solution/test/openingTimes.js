const mongoose = require(`mongoose`);
const OpeningTime = require(`../models/openingTime.model`);

const chai = require(`chai`);
const chaiHttp = require(`chai-http`);
const should = chai.should();
const server = require(`../server`);

testData = require(`./testData/sampleOpenings`);

const TESTPATH = `/openingTimes`;

chai.use(chaiHttp);

describe(`Test of retrieving opening times`, () => {
    beforeEach(done => {
        OpeningTime.deleteMany({}, err => {
            if (err) { done() };
        });

        OpeningTime.insertMany(testData.openings, (err, res) => {
            if (err) {
                console.info(`Error inserting`);
                done();
            } else {
                console.info(`Documents inserted`);
                done();
            }
        });
    });

    it(`should retrieve all of the sample opening times`, done => {
        chai.request(server)
            .get(TESTPATH)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.an('array');
                res.body.length.should.eql(3);
                done();
            });
    });
});