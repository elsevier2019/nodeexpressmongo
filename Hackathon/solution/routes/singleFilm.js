const express = require(`express`);
const router = express.Router();
const Film = require(`../models/film.model`);
const { mongoIdRegExp } = require(`../js/regExps`);

router.route(`/:id`).get((req, res) => {
    const id = req.params.id;
    if (!id.match(mongoIdRegExp)) {
        return res.status(422).json({
            'message': `There was a problem with the film Id`,
        });
    }
    Film.findById(id, (error, film) => {
        (error || !film) ? res.status(404).json({ 'message': `Not found` }) : res.json(film);
    });
});

module.exports = router;