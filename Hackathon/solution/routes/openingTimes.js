const express = require(`express`);
const router = express.Router();
const OpeningTime = require(`../models/openingTime.model`);

router.route(`/`).get((req, res) => {
    OpeningTime.find((error, openings) => {
        error ? res.status(404).json({ 'message': `Not found` }) : res.json(openings);
    });
});

module.exports = router;
