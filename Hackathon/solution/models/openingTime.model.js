const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;
//const { hhmmRegExp } = require(`../js/regExps`);

const OpeningTime = new Schema({
    day: { type: String, required: true },
    opening: { type: String, required: true /*, match: [hhmmRegExp, `is invalid`] */ },
    close: { type: String, required: true /*, match: [hhmmRegExp, `is invalid`] */ },
});

module.exports = mongoose.model(`OpeningTime`, OpeningTime);