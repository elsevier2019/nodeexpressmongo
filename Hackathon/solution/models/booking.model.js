const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const { isoDateRegExp, emailRegExp } = require(`../js/regExps`);

const Booking = new Schema({
    filmId: { type: ObjectId, required: true },
    bookingDate: { type: Date, required: true, match: [isoDateRegExp, `is invalid`] },
    email: { type: String, required: true, match: [emailRegExp, `is invalid`] },
    adults: { type: Number, min: 0 },
    child: { type: Number, min: 0 },
    concessions: { type: Number, min: 0 }
});

module.exports = mongoose.model(`Booking`, Booking);